export const idCurrentElection = '92';
export const superPartyBlockId = { //Los partidos sin colores en Zeplin, como 'Recortes Cero'
    10: {alias: 'Cs', largeAlias: 'Cs', lightColor: '#ffc09f', color: '#f45a09', indepe: 'no_independentista', cssClass: 'table__partido--cs'}, //Cs
    17: {alias: 'ERC', largeAlias: 'ERC', lightColor: '#fde0af', color: '#f9b33c', indepe: 'independentista', cssClass: 'table__partido--erc'}, //ERC
    691: {alias: 'JxCAT', largeAlias: 'JxCAT', lightColor: '#bae8e4', color: '#20c0b2', indepe: 'independentista', cssClass: 'table__partido--jxcat'}, //JXC
    40: {alias: 'PSC', largeAlias: 'PSC', lightColor: '#fbc9c7', color: '#f60b01', indepe: 'no_independentista', cssClass: 'table__partido--psc'}, //PSC
    80: {alias: 'En Comú', largeAlias: 'En Comú Podem', lightColor: '#d7a7dd', color: '#692772', indepe: 'no_independentista', cssClass: 'table__partido--ecp'}, //Comunes
    11027: {alias: 'CUP', largeAlias: 'CUP', lightColor: '#fcf9ca', color: '#fff200', indepe: 'independentista', cssClass: 'table__partido--cup'}, //CUP
    38: {alias: 'PP', largeAlias: 'PP', lightColor: '#c6e5ff', color: '#48aafd', indepe: 'no_independentista', cssClass: 'table__partido--pp'}, //PP
    43: {alias: 'Recortes', largeAlias: 'Recortes Cero-GV-M', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'}, //Recortes 0
    65: {alias: 'PUM+J', largeAlias: 'PUM+J', lightColor: '#d6e3e7', color: '#7ec2d6', indepe: 'no_independentista', cssClass: 'table__partido--pumj'}, //PUM+J
    52: {alias: 'VOX', largeAlias: 'VOX', lightColor: '#d5e9c0', color: '#77be2d', indepe: 'no_independentista', cssClass: 'table__partido--vox'}, //VOX
    7: {alias: 'PDeCAT', largeAlias: 'PDeCAT', lightColor: '#8daacd', color: '#16447b', indepe: 'independentista', cssClass: 'table__partido--pdcat'}, //PDCAT
    4100004: {alias: 'PNC', largeAlias: 'PNC', lightColor: '#a5d0db', color: '#2da8b8', indepe: 'independentista', cssClass: 'table__partido--pnc'}, //PNC
    4100008: {alias: "L'EBRE", largeAlias: "SOM TERRES L'EBRE", lightColor: '#c8c8c8', color: '#575757', indepe: 'independentista', cssClass: 'table__partido--recortes'},
    708: {alias: 'PCTC', largeAlias: 'PCTC (Partido Comunista)', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    12: {alias: 'EB', largeAlias: 'Escaños en Blanco', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    687: {alias: 'IZQP', largeAlias: 'Izquierda en Positivo', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    636: {alias: 'FNC', largeAlias: 'Front Nacional de Catalunya', lightColor: '#c8c8c8', color: '#575757', indepe: 'independentista', cssClass: 'table__partido--recortes'},
    4100003: {alias: 'MPIC', largeAlias: 'Primaries per la Indepèndencia', lightColor: '#c8c8c8', color: '#575757', indepe: 'independentista', cssClass: 'table__partido--recortes'},
    3000882: {alias: 'M.C.R', largeAlias: 'Moviment Corrent Roig', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    4100002: {alias: 'UN. SÍ', largeAlias: 'Unidos Sí-DEF-PDSJE-Somos España', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    4100005: {alias: 'UEP', largeAlias: 'Unión Europea de Pensionistas', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    4100006: {alias: 'AL.CV', largeAlias: 'Alianza por el Comercio y la Vivienda', lightColor: '#c8c8c8', color: '#575757', indepe: 'no_independentista', cssClass: 'table__partido--recortes'},
    4100007: {alias: 'SCAT', largeAlias: 'Suport Civil Català', lightColor: '#c8c8c8', color: '#575757', indepe: 'independentista', cssClass: 'table__partido--recortes'}
}