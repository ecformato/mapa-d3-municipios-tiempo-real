export function isMobile() {
    return window.innerWidth < 525; //Cambiar cuando sepamos corte (en widget de portada está en 485)
}

export function getNumberWithThousandsPoint(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}