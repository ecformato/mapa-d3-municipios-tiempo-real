import '@babel/polyfill';
import { isMobile } from './helpers/dom-logic';
import { mobileDevelopment } from './main-components/mobile';
import { desktopDevelopment } from './main-components/desktop';

import '../css/main.scss';

//Diferencia desarrollos
if(isMobile()){
    mobileDevelopment();
} else {
    desktopDevelopment();
}