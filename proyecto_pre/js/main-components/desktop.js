//Uso de librería d3
import { json } from 'd3-request';
import { queue } from 'd3-queue';
import { select, event } from 'd3-selection';
import { geoPath } from 'd3-geo';
import { geoConicConformalSpain } from 'd3-composite-projections';
import { feature } from 'topojson-client';
import { zoom as zoomInit, zoomIdentity } from 'd3-zoom';

import { initAutocomplete } from '../helpers/autocomplete';
import { superPartyBlockId, idCurrentElection } from '../helpers/parties';
import { scaleLinear } from 'd3-scale';

export let posicionInicial, zoomCity;

//CONSTANTES
const CHANGEUP = '#A7C5D8';
const CHANGEDOWN = '#F35E79';
const WITHOUTDATA = '#d7d7d7';  

export async function desktopDevelopment() {
    //Datos globales a ambas cargas > Para nutrir de datos a las mismas variables
    let zoomed, stopped;
    let map, currentCityZoomed, svg, g, g2, zoom, currentDataVisible;
    let mapWidth = document.getElementById('map').clientWidth, mapHeight = 350;
    let mapFeatures = [], arrayNombreMunicipios = [];
    let tooltip;
    let participacion, escrutado;
    let citySelected = '';

    let currentButton = 'primeraFuerza';
    let currentFilter = 'year2021';

    ///////
    //// CARGA DE INFORMACIÓN > DOS OPCIONES
    ///////

    //OPCIÓN 2 >>>>> Una única llamada asíncrona
    uploadEveryData().then(function(){
        initEvents();
        initZoomEvents();
    });
    uploadGeneralProps();

    async function uploadEveryData() {
        let q = queue();
        q.defer(json, "https://raw.githubusercontent.com/CarlosMunozDiaz/prueba_datos_traductor/main/municipios_id_ine.json");
        //Mapa de provincias
        q.defer(json, "https://raw.githubusercontent.com/CarlosMunozDiaz/prueba_datos_traductor/main/municipios_cat_mapshaper_v3.json");
        q.defer(json, 'https://raw.githubusercontent.com/CarlosMunozDiaz/prueba_datos_traductor/main/provincias_cat_mapshaper.json');
        //Llamadas datos para mapa
        q.defer(json, `https://api.elconfidencial.com/service/electionsscytl/map/${idCurrentElection}/5/`);
        q.defer(json, "https://api.elconfidencial.com/service/electionsscytl/map/88/5/");

        q.await(function(error, nombreMunicipios, poligonos, poligonosProvincia, datos2021, datos2017) {
            if(error) throw error;

            //Previo al mapa > Tooltip y buscador
            arrayNombreMunicipios = [...nombreMunicipios];
            initData();

            /*
            * MAPA
            */            
            map = select("#map");
            currentCityZoomed = '';
        
            svg = map.append("svg")
                .attr("width", mapWidth)
                .attr("height", mapHeight)
                .on("click", stopped, true);
        
            ///////
            //// DIBUJO INICIAL DEL MAPA
            ///////
        
            //Polígonos provinciales
            let esProv = poligonosProvincia;
            let usProv = feature(esProv, esProv.objects.provincias);
        
            //Polígonos municipales
            let es = poligonos;
            let us = feature(es, es.objects.municipios);            
            
            const projection = geoConicConformalSpain().scale(2000).fitSize([mapWidth,mapHeight], us);
            const path = geoPath(projection);
            
            mapFeatures = [...us.features];
            let apiData = datos2021.data;
            let apiData2017 = datos2017.data;
            
            mapFeatures.forEach(function(item){
                item.properties.centroid = path.centroid(item);
                //2021
                let apiProperties2021 = apiData.filter(function(itemApi) {
                    if(item.properties.CODIMUNI.substr(0,5) == itemApi.id) {
                        return itemApi;
                    }
                })[0];
                item.apiProperties2021 = apiProperties2021;
                //2017
                let apiProperties2017 = apiData2017.filter(function(itemApi) {
                    if(item.properties.CODIMUNI.substr(0,5) == itemApi.id) {
                        return itemApi;
                    }
                })[0];
                item.apiProperties2017 = apiProperties2017;
                //////
                //Otros elementos
                //////
                //Independentismo
                let auxPorcIndepe = 0, auxPorcNoIndepe = 0;
                if(item.apiProperties2021){
                    for(let i = 0; i < item.apiProperties2021.parties.length; i++) {
                        let item2 = item.apiProperties2021.parties[i];
                        let tipoIndepe = superPartyBlockId[item2.par_meta_id] ? superPartyBlockId[item2.par_meta_id].indepe : null;
                        if (tipoIndepe) {
                            if(tipoIndepe == 'independentista'){
                                auxPorcIndepe += item2.par_percent;
                            } else {
                                auxPorcNoIndepe += item2.par_percent;
                            }
                        }
                    }
                    item.independentismo = auxPorcIndepe > auxPorcNoIndepe ? 'independentista' : auxPorcIndepe < auxPorcNoIndepe ? 'no_indepe' : 'tablas';
                    item.porcIndependentista = auxPorcIndepe;
                    item.porcNoIndependentista = auxPorcNoIndepe;
                } else {
                    item.independentismo = null;
                    item.porcIndependentista = 0;
                    item.porcNoIndependentista = 0;
                }

                if(item.apiProperties2021) {
                    //Cambio PSC
                    let psc2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 40)[0] : 0;
                    let psc2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 40)[0];
                    let difPsc = 0;                
                    if(psc2021Exist){
                        difPsc = psc2021Exist.par_percent - (psc2017Exist ? psc2017Exist.par_percent : 0);
                    } else {
                        difPsc = 0 - (psc2017Exist ? psc2017Exist.par_percent : 0);
                    }
                    item.changePsc = difPsc; 

                    //Cambio ERC
                    let erc2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 17)[0] : 0;
                    let erc2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 17)[0];
                    let difErc = 0;
                    if(erc2021Exist){
                        difErc = erc2021Exist.par_percent - (erc2017Exist ? erc2017Exist.par_percent : 0);
                    } else {
                        difErc = 0 - (erc2017Exist ? erc2017Exist.par_percent : 0);
                    }
                    item.changeErc = difErc;

                    //Cambio PP
                    let pp2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 38)[0] : 0;
                    let pp2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 38)[0];
                    let difPp = 0;
                    if(pp2021Exist){
                        difPp = pp2021Exist.par_percent - (pp2017Exist ? pp2017Exist.par_percent : 0);
                    } else {
                        difPp = 0 - (pp2017Exist ? pp2017Exist.par_percent : 0);
                    }
                    item.changePp = difPp;

                    //Cambio CS
                    let cs2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 10)[0] : 0;
                    let cs2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 10)[0];
                    let difCs = 0;
                    if(cs2021Exist){
                        difCs = cs2021Exist.par_percent - (cs2017Exist ? cs2017Exist.par_percent : 0);
                    } else {
                        difCs = 0 - (cs2017Exist ? cs2017Exist.par_percent : 0);
                    }
                    item.changeCs = difCs;

                    //Cambio JxCAT
                    let jxc2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 691)[0] : 0;
                    let jxc2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 691)[0];
                    let difJxc = 0;
                    if(jxc2021Exist){
                        difJxc = jxc2021Exist.par_percent - (jxc2017Exist ? jxc2017Exist.par_percent : 0);
                    } else {
                        difJxc = 0 - (jxc2017Exist ? jxc2017Exist.par_percent : 0);
                    }
                    item.changeJxc = difJxc;

                    //Cambio CUP
                    let cup2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 11027)[0] : 0;
                    let cup2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 11027)[0];
                    let difCup = 0;
                    if(cup2021Exist){
                        difCup = cup2021Exist.par_percent - (cup2017Exist ? cup2017Exist.par_percent : 0);
                    } else {
                        difCup = 0 - (cup2017Exist ? cup2017Exist.par_percent : 0);
                    }
                    item.changeCup = difCup;

                    //Cambio Ec Comú
                    let podem2021Exist = item.apiProperties2021?.parties ? item.apiProperties2021.parties.filter(item => item.par_meta_id == 80)[0] : 0;
                    let podem2017Exist = item.apiProperties2017.parties.filter(item => item.par_meta_id == 80)[0];
                    let difPodem = 0;
                    if(podem2021Exist){
                        difPodem = podem2021Exist.par_percent - (podem2017Exist ? podem2017Exist.par_percent : 0);
                    } else {
                        difPodem = 0 - (podem2017Exist ? podem2017Exist.par_percent : 0);
                    }
                    item.changePodem = difPodem;

                    //Cambio VOX
                    item.changeVox = null;
                }                
            });

            currentDataVisible = 'fuerzas2021';
        
            zoom = zoomInit().scaleExtent([1, 7]).translateExtent([[0,0], [mapWidth,350]]).on('zoom', zoomed);
            svg.call(zoom);
        
            g = svg
                .append('g')
                .attr('id', 'gmap');

            g2 = svg.append('g');
            
            //Estado inicial > Primera fuerza de 2021
            g.selectAll("path")
                .data(mapFeatures)
                .enter()
                .append("path")
                .attr("d", path)
                .attr("class", 'region-mun')
                .attr("id", function(d){return d.properties.CODIMUNI.substr(0,5)})
                .style("fill", function(d){
                    if(d.apiProperties2021){
                        return superPartyBlockId[d.apiProperties2021.parties[0].par_meta_id].color;
                    } else {
                        return WITHOUTDATA;
                    }                    
                })
                .style('opacity', 1)
                .style("stroke", "#fff")
                .style("stroke-width", "0.2px")
                .on('click', function(){
                    zoomCity(this.id);
                });
        
            g2.selectAll("path-prov")
                .data(usProv.features)
                .enter()
                .append("path")
                .attr("d", path)
                .attr("class", 'region-prov')
                .style("fill", 'none')
                .style('opacity', 1)
                .style("stroke", "#fff")
                .style("stroke-width", ".75px");
        });
    }

    async function uploadGeneralProps() {
        //Llamada API
        window.fetch(`https://api.elconfidencial.com/service/electionsscytl/place/${idCurrentElection}/CA09/`)
        .then(function(response){
            return response.json()
        }).then(function(data){
            let electionProps = data.data.places.CA09;
            participacion = electionProps.participation ? electionProps.participation.par_percent_total_votes : 0;
            escrutado = electionProps.participation ? electionProps.participation.par_percent_counted : 0;
            document.getElementById('porcParticipacion').textContent = participacion + '%';
            document.getElementById('porcEscrutado').textContent = escrutado + '%';

            document.getElementById('barraParticipacion').style.width = participacion + '%';
            document.getElementById('barraEscrutado').style.width = escrutado  + '%';
        });
    }

    /*
    * Helpers para inicialización de elementos
    */
    function initData() {
        /*
        *
        * TOOLTIP
        *
        */
        tooltip = select('#map')
            .append('div')
            .attr('class', 'cat-tooltip')
            .style("opacity", 0);
   
       /*
       *
       * AUTOCOMPLETADO
       *
       */
       initAutocomplete(arrayNombreMunicipios);
    }

    function initEvents() {
        //Lógica para los cuatro estados de botones
        document.getElementById('year2021').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'year2021';
            
            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);

            //Cambio de lógica en función del botón seleccionado
            if (currentButton == 'primeraFuerza') {
                drawPrimeraFuerza2021();
            } else if (currentButton == 'segundaFuerza') {
                drawSegundaFuerza2021();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('year2017').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'year2017';

            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);
            setLegend();

            //Cambio de lógica en función del botón seleccionado
            if(currentButton == 'primeraFuerza') {
                drawPrimeraFuerza2017();
            } else if (currentButton == 'segundaFuerza') {
                drawSegundaFuerza2017();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('dif2021').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'dif2021';

            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);
            setLegend(currentGradient);

            //Cambio de lógica en función del botón seleccionado
            if(currentButton == 'psc') {
                drawPsc2021();
            } else if (currentButton == 'erc') {
                drawErc2021();
            } else if (currentButton == 'jxc') {
                drawJxc2021();
            } else if (currentButton == 'cup') {
                drawCup2021();
            } else if (currentButton == 'vox') {
                drawVox2021();
            } else if (currentButton == 'pp') {
                drawPP2021();
            } else if (currentButton == 'cs') {
                drawCS2021();
            } else if (currentButton == 'podem') {
                drawPodem2021();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('dif2017').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'dif2017';

            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);
            setLegend();

            //Cambio de lógica en función del botón seleccionado
            if(currentButton == 'psc') {
                drawPscChange();
            } else if (currentButton == 'erc') {
                drawErcChange();
            } else if (currentButton == 'jxc') {
                drawJxcChange();
            } else if (currentButton == 'cup') {
                drawCupChange();
            } else if (currentButton == 'vox') {
                drawVoxChange();
            } else if (currentButton == 'pp') {
                drawPPChange();
            } else if (currentButton == 'cs') {
                drawCSChange();
            } else if (currentButton == 'podem') {
                drawPodemChange();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        //Lógica para los botones de filtros y partidos
        document.getElementById('primeraFuerza').addEventListener('click', function(e) {
            if(currentFilter == 'year2017'){
                drawPrimeraFuerza2017();
                setFilter('year2017');
            } else {
                drawPrimeraFuerza2021();
                setFilter('year2021');
            }
            setBlock('year');
            setButton(e.target);
            setLegend();

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('segundaFuerza').addEventListener('click', function(e) {
            if(currentFilter == 'year2017'){
                drawSegundaFuerza2017();
                setFilter('year2017');
            } else {
                drawSegundaFuerza2021();
                setFilter('year2021');
            }
            
            setBlock('year');
            setButton(e.target);
            setLegend();

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('bloqueIndependencia').addEventListener('click', function(e) {
            drawBloqueIndependencia();
            //Aquí tendríamos que dejar disabled los botones (los dos bloques) y pintar una leyenda
            setFilter();
            setBlock();
            setButton(e.target);
            setLegend();

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('psc').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawPscChange();
                setFilter('dif2017');
            } else {
                drawPsc2021();
                setFilter('dif2021');
                setLegend('psc');     
            }   
                
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'psc';                     
        });

        document.getElementById('erc').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawErcChange();
                setFilter('dif2017');
            } else {
                drawErc2021();
                setFilter('dif2021'); 
                setLegend('erc');               
            }
            
            setBlock('dif');
            setButton(e.target);  

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
            
            currentGradient = 'erc';
        });

        document.getElementById('jxc').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawJxcChange();
                setFilter('dif2017');
            } else {
                drawJxc2021();
                setFilter('dif2021');
                setLegend('jxc');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'jxc';
        });

        document.getElementById('cup').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawCupChange();
                setFilter('dif2017');
            } else {
                drawCup2021();
                setFilter('dif2021');
                setLegend('cup');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'cup';
        });

        document.getElementById('pp').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawPPChange();
                setFilter('dif2017');
            } else {
                drawPP2021();
                setFilter('dif2021');
                setLegend('pp');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'pp';
        });

        document.getElementById('cs').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawCSChange();
                setFilter('dif2017');
            } else {
                drawCS2021();
                setFilter('dif2021');
                setLegend('cs');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'cs';
        });

        document.getElementById('podem').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawPodemChange();
                setFilter('dif2017');
            } else {
                drawPodem2021();
                setFilter('dif2021');
                setLegend('podem');
            }
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'podem';
        });

        document.getElementById('vox').addEventListener('click', function(e) {
            if(currentFilter == 'dif2017'){
                drawVoxChange();
                setFilter('dif2017');
            } else {
                drawVox2021();
                setFilter('dif2021');
                setLegend('vox');
            }
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }

            currentGradient = 'vox';
        });
    }

    function initZoomEvents() {
        ////////
            //// EVENTOS ZOOM
            ////////
            zoomed = function() {
                if(event.transform.k < 5) {
                    getOutTooltip();
                } else {
                    if(citySelected != ''){
                        showTooltip();
                    }
                }
                svg.selectAll('path').attr('transform', event.transform);
            }
        
            posicionInicial = function(){
                //ESTILOS
                if(currentCityZoomed != ''){
                    currentCityZoomed.style.strokeWidth = '0.2px';
                    currentCityZoomed.style.stroke = '#fff';
                    currentCityZoomed = '';
                }
                //ZOOM
                svg.transition()
                    .duration(1200)
                    .call(zoom.transform, zoomIdentity.scale(1));
        
                //TOOLTIP
                tooltip
                    .transition()     
                    .duration(200)
                    .style('display', 'none');
            }
            
            zoomCity = function(city) {
                let ciudad = mapFeatures.filter(function(item){if (item.properties.CODIMUNI.substr(0,5) == city){return item;}})[0];
                let ciudad2 = document.getElementById(`${city}`);

                let aux = select(ciudad2).node();

                document.getElementById('gmap').removeChild(ciudad2);
                document.getElementById('gmap').appendChild(aux);

                aux.addEventListener('click', function(){
                    zoomCity(this.id);
                });
                                
                //ESTILOS
                if(currentCityZoomed != ''){
                    currentCityZoomed.style.stroke = '#fff';
                    currentCityZoomed.style.strokeWidth = '0.2px';
                }

                aux.style.strokeWidth = '0.4px';
                aux.style.stroke = '#7f7f7f';

                currentCityZoomed = ciudad2;
        
                //ZOOM
                let x = ciudad.properties.centroid[0];
                let y = ciudad.properties.centroid[1];
                let k = 6.5;
                let translate = [mapWidth / 2 - k * x, mapHeight / 2 - k * y];
                
                svg.transition()
                    .duration(1000)
                    .call(zoom.transform, zoomIdentity.translate(translate[0],translate[1]).scale(k));

                //Evento del tooltip
                citySelected = ciudad;
                setTooltip(ciudad);
            }
        
            stopped = function() {
                if (event.defaultPrevented) {
                    event.stopPropagation();
                } 
            } 
    }

    /* 
    * Seteo de información del mapa 
    */
    let filterBlocks = document.getElementsByClassName('map-filters__item');
    let filterElements = document.getElementsByClassName('btn--filter');
    let optionsElements = document.getElementsByClassName('btn--option-js');
    let partiesLegend = document.getElementsByClassName('map-info')[0];
    let currentGradient = 'psc';
    let gradientBar = document.getElementsByClassName('gradient-box__value')[0];
    
    function setFilter(btnId) {        
        for(let i = 0; i < filterElements.length; i++) {
            let item = filterElements[i];

            if(item.classList.contains('btn--active')){
                item.classList.remove('btn--active');
            }          
        }

        if(btnId) {
            let btn = document.getElementById(btnId);
            btn.classList.add('btn--active');
        } else {
            currentFilter = 'year2021';
        }  
    }

    function setBlock(tipo) {
        for(let i = 0; i < filterBlocks.length; i++){
            let item = filterBlocks[i];
            item.classList.remove('map-filters--active');
        }
        if(tipo == 'year'){            
            filterBlocks[0].classList.add('map-filters--active');
        } else if (tipo == 'dif') {
            filterBlocks[1].classList.add('map-filters--active');
        } else {
            filterBlocks[2].classList.add('map-filters--active');
        }
    }

    function setButton(btn) {
        for(let i = 0; i < optionsElements.length; i++){
            let item = optionsElements[i];

            item.classList.remove('btn--active');
        }

        if(btn) {
            btn.classList.add('btn--active');
        }
    }

    function setLegend(party) {
        // 1) Cambiamos el color del gradiante en función del partido
        // 2) Hacemos visible o no visible el bloque
        if(party) {
            gradientBar.classList.remove(`gradient-box__value--${currentGradient}`);
            gradientBar.classList.add(`gradient-box__value--${party}`);
            currentGradient = party;

            if(!partiesLegend.classList.contains('map-info--active')){
                partiesLegend.classList.add('map-info--active');
            }            
        } else {
            partiesLegend.classList.remove('map-info--active');
            gradientBar.classList.remove(`gradient-box__value--${currentGradient}`);
        }        
    }

    /* 
    * 
    * Helpers de visualización del mapa
    *  
    */
    function drawPrimeraFuerza2021() {
        currentButton = 'primeraFuerza';
        currentDataVisible = 'fuerzas2021';

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.apiProperties2021){
                    return superPartyBlockId[d.apiProperties2021.parties[0].par_meta_id].color;
                } else {
                    return WITHOUTDATA;
                }  
            });
    }

    function drawPrimeraFuerza2017() {
        currentButton = 'primeraFuerza';
        currentDataVisible = 'fuerzas2017';

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.apiProperties2017){
                    return superPartyBlockId[d.apiProperties2017.parties[0].par_meta_id].color;
                } else {
                    return WITHOUTDATA;
                }  
            });
    }

    function drawSegundaFuerza2021() {
        currentButton = 'segundaFuerza';
        currentDataVisible = 'fuerzas2021';

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.apiProperties2021 && d.apiProperties2021.parties[1]){
                    return superPartyBlockId[d.apiProperties2021.parties[1].par_meta_id].color;
                } else {
                    return WITHOUTDATA;
                }  
            });
    }

    function drawSegundaFuerza2017() {
        currentButton = 'segundaFuerza';
        currentDataVisible = 'fuerzas2017';

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.apiProperties2017){
                    return superPartyBlockId[d.apiProperties2017.parties[1].par_meta_id].color;
                } else {
                    return WITHOUTDATA;
                }  
            });
    }

    function drawBloqueIndependencia() {
        currentButton = 'bloqueIndependencia';
        currentDataVisible = 'bloqueIndependencia';

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                    return d.independentismo ? 
                        d.independentismo == 'no_indepe' ? '#000' : 
                        d.independentismo == 'independentista' ? '#e0e400' : 
                        '#e3e2d8' : WITHOUTDATA;
            });
    }

    function drawPsc2021() {
        currentButton = 'psc';
        currentDataVisible = 'psc';        
            
        let colorPsc = scaleLinear().domain([-1,40]).range([superPartyBlockId[40].lightColor,superPartyBlockId[40].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoPsc;
                if(d.apiProperties2021) {
                    datoPsc = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 40)[0] : 0;
                } 
                return datoPsc ? colorPsc(datoPsc.par_percent) : colorPsc(0);
            });
    }

    console.log("entra");

    function drawPscChange() {
        currentButton = 'psc';
        currentDataVisible = 'psc';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changePsc ? d.changePsc > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });

        // tooltip
        //     .transition()     
        //     .duration(200)
        //     .style('display', 'none');
    }

    function drawErc2021() {
        currentButton = 'erc';
        currentDataVisible = 'erc';
            
        let colorErc = scaleLinear().domain([-1,40]).range([superPartyBlockId[17].lightColor,superPartyBlockId[17].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoErc;

                if(d.apiProperties2021){
                    datoErc = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 17)[0] : 0;
                } 
                return datoErc ? colorErc(datoErc.par_percent) : colorErc(0);
            });
    }

    function drawErcChange() {
        currentButton = 'erc';
        currentDataVisible = 'erc';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changeErc ? d.changeErc > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });

        // tooltip
        //     .transition()     
        //     .duration(200)
        //     .style('display', 'none');
    }

    function drawJxc2021() {
        currentButton = 'jxc';
        currentDataVisible = 'jxc';
            
        let colorJxc = scaleLinear().domain([0,40]).range([superPartyBlockId[691].lightColor,superPartyBlockId[691].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoJxc;

                if(d.apiProperties2021){
                    datoJxc = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 691)[0] : 0;
                }
                return datoJxc ? colorJxc(datoJxc.par_percent) : colorJxc(0);
            });
    }

    function drawJxcChange() {
        currentButton = 'jxc';
        currentDataVisible = 'jxc';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changeJxc ? d.changeJxc > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });
    }

    function drawCup2021() {
        currentButton = 'cup';
        currentDataVisible = 'cup';
            
        let colorCup = scaleLinear().domain([-1,40]).range([superPartyBlockId[11027].lightColor,superPartyBlockId[11027].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoCup;

                if(d.apiProperties2021){
                    datoCup = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 11027)[0] : 0;
                }
                return datoCup ? colorCup(datoCup.par_percent) : colorCup(0);
            });
    }

    function drawCupChange() {
        currentButton = 'cup';
        currentDataVisible = 'cup';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changeCup ? d.changeCup > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });

        // tooltip
        //     .transition()     
        //     .duration(200)
        //     .style('display', 'none');
    }

    function drawPP2021() {
        currentButton = 'pp';
        currentDataVisible = 'pp';
            
        let colorPp = scaleLinear().domain([0,40]).range([superPartyBlockId[38].lightColor,superPartyBlockId[38].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoPp;

                if(d.apiProperties2021){
                    datoPp = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 38)[0] : 0;
                }
                return datoPp ? colorPp(datoPp.par_percent) : colorPp(0);
            });
    }

    function drawPPChange() {
        currentButton = 'pp';
        currentDataVisible = 'pp';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changePp ? d.changePp > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });
    }

    function drawPodem2021() {
        currentButton = 'podem';
        currentDataVisible = 'podem';
            
        let colorPodem = scaleLinear().domain([0,40]).range([superPartyBlockId[80].lightColor,superPartyBlockId[80].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoPodem;
                
                if(d.apiProperties2021){
                    datoPodem = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 80)[0] : 0;
                }
                
                return datoPodem ? colorPodem(datoPodem.par_percent) : colorPodem(0);
            });
    }

    function drawPodemChange() {
        currentButton = 'podem';
        currentDataVisible = 'podem';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changePodem ? d.changePodem > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });
    }

    function drawCS2021() {
        currentButton = 'cs';
        currentDataVisible = 'cs';
            
        let colorCs = scaleLinear().domain([0,40]).range([superPartyBlockId[10].lightColor,superPartyBlockId[10].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoCs;
                
                if(d.apiProperties2021){
                    datoCs = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 10)[0] : 0;
                } 
                return datoCs ? colorCs(datoCs.par_percent) : colorCs(0);
            });
    }

    function drawCSChange() {
        currentButton = 'cs';
        currentDataVisible = 'cs';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                return d.changeCs ? d.changeCs > 0 ? CHANGEUP : CHANGEDOWN : WITHOUTDATA;
            });
    }

    function drawVox2021() {
        currentButton = 'vox';
        currentDataVisible = 'vox';
            
        let colorVox = scaleLinear().domain([0,40]).range([superPartyBlockId[52].lightColor,superPartyBlockId[52].color]);

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoVox;
                if(d.apiProperties2021) {
                    datoVox = d.apiProperties2021?.parties ? d.apiProperties2021.parties.filter(item => item.par_meta_id == 52)[0] : 0;
                }

                return datoVox ? colorVox(datoVox.par_percent) : colorVox(0);
            });
    }

    function drawVoxChange() {
        currentButton = 'vox';
        currentDataVisible = 'vox';           

        g.selectAll(".region-mun")
            .transition()
            .duration(800)
            .style("fill", WITHOUTDATA);
    }

    /* Pintado de tooltip */
    function drawTooltip(id, data) {
        let html = '<span id="close-tooltip">X</span>';
        
        //Información para todos los bloques
        let provincia = data.properties.CODIPROV == '08' ? 'Barcelona' : 
            data.properties.CODIPROV == '17' ? 'Girona' :
            data.properties.CODIPROV == '25' ? 'Lleida' : 'Tarragona';

        let munWithoutSpace = data.properties.NOMMUNI.trim();
        let firstMun = munWithoutSpace.substr(0,1).toUpperCase();
        let restMun = munWithoutSpace.substr(1,);
        let totalMun = firstMun.concat(restMun);
        let municipio = `<span><b>${totalMun} (${provincia})</b></span>`;
        
        if (id == 'fuerzas2021') {
            if(data.apiProperties2021){
                let segundaFuerza = data.apiProperties2021.parties[1] ? data.apiProperties2021.parties[1] : null;
                if(data)
                html += `${municipio}
                    <span>Primera fuerza: ${data.apiProperties2021.parties[0].par_alias} (${data.apiProperties2021.parties[0].par_percent}%)</span>`;
                if(segundaFuerza) {
                    html += `<span>Segunda fuerza: ${data.apiProperties2021.parties[1].par_alias} (${data.apiProperties2021.parties[1].par_percent}%)</span>`;
                } else {
                    html += `Segunda fuerza: No hay`;
                }                   
            } else {
                html += `${municipio}
                    <span>Sin información</span>`;
            }            
        } else if (id == 'fuerzas2017') {
            if(data.apiProperties2017){
                html += `${municipio}
                    <span>Primera fuerza: ${data.apiProperties2017.parties[0].par_alias} (${data.apiProperties2017.parties[0].par_percent}%)</span>
                    <span>Segunda fuerza: ${data.apiProperties2017.parties[1].par_alias} (${data.apiProperties2017.parties[1].par_percent}%)</span>`;
            } else {
                html += `${municipio}
                    <span>Sin información</span>`;
            }   
        } else if (id == 'bloqueIndependencia') {
            if(data.independentismo){
                html += `${municipio}
                    <span>Voto indep.: ${data.porcIndependentista.toFixed(2).replace('.',',')}%</span>
                    <span>Voto no indep.: ${data.porcNoIndependentista.toFixed(2).replace('.',',')}%</span>`;
            } else {
                html += `${municipio}
                    <span>Sin información</span>`;
            } 
        } else {
            if(data.apiProperties2021){
                let partido = '-', dato2021 = '-', diferencia = '-';
                if(id == 'psc'){
                    partido = 'PSC';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 40)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changePsc.toFixed(1);
                } else if (id == 'pp') {
                    partido = 'PP';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 38)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changePp.toFixed(1);
                } else if (id == 'cup') {
                    partido = 'CUP';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 11027)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changeCup.toFixed(1);
                } else if (id == 'erc') {
                    partido = 'ERC';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 17)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changeErc.toFixed(1);
                } else if (id == 'jxc') {
                    partido = 'JxCAT';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 691)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changeJxc.toFixed(1);
                } else if (id == 'vox') {
                    partido = 'VOX';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 52)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = '-';
                } else if (id == 'podem') {
                    partido = 'Cat Comú';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 80)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changePodem.toFixed(1);
                } else if (id == 'cs') {
                    partido = 'CS';
                    dato2021 = data.apiProperties2021.parties.filter(item => item.par_meta_id == 10)[0];
                    dato2021 = dato2021 ? dato2021.par_percent.toFixed(1) + '%' : '0%';
                    diferencia = data.changeCs.toFixed(1);
                }

                html += `${municipio}
                    <span>${partido}: ${dato2021}</span>
                    <span>Dif. con 2017: ${diferencia}</span>`;
                } else {
                    html += `${municipio}
                        <span>Sin información</span>`;
            }  
        }

        return html;
    }

    function setTooltip(d) {
        let result = drawTooltip(currentDataVisible, d);                    
        tooltip.html(result);

        document.getElementById('close-tooltip').addEventListener('click', function () {
            getOutTooltip();
            citySelected = '';
            if(currentCityZoomed != ''){
                currentCityZoomed.style.stroke = '#fff';
                currentCityZoomed.style.strokeWidth = '0.2px';
            }
            currentCityZoomed = '';
        });              
    }

    function getOutTooltip() {
        tooltip
            .transition()     
            .duration(200)
            .style('display', 'none');
    }
    
    function showTooltip() {
        tooltip
            .transition()     
            .duration(200)
            .style('display', 'block')
            .style('opacity','0.9');
    }
}